package com.wallmart.logistica.domain;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

public class RotaCompletaTest {

	@Test
	public void calcularDistanciaTotalSemSubRotas() {
		RotaCompleta rc = new RotaCompleta();
		
		rc.calcularDistanciaTotal();
		
		Assert.assertEquals(new Integer(0), rc.getDistanciaTotal());
	}
	
	@Test
	public void calcularDistanciaTotalComSubRotas() {
		RotaCompleta rc = new RotaCompleta();
		
		Rota rota1 = new Rota();
		rota1.setDistancia(123);
		
		Rota rota2 = new Rota();
		rota2.setDistancia(567);
		
		rc.setSubRotas(Arrays.asList(rota1, rota2));
		rc.calcularDistanciaTotal();
		
		Assert.assertEquals(new Integer(690), rc.getDistanciaTotal());
	}
	
}
