package com.wallmart.logistica.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.wallmart.logistica.domain.Mapa;
import com.wallmart.logistica.domain.Ponto;
import com.wallmart.logistica.repository.MapaRepository;
import com.wallmart.logistica.service.impl.CalculoRotaServiceImpl;

public class CalculoRotaServiceTest {
	
	private CalculoRotaService calculoRota;
	private MapaRepository mockRepository;
	
	@Before
	public void init() {
		mockRepository = Mockito.mock(MapaRepository.class);
		calculoRota = new CalculoRotaServiceImpl() {{
			this.mapaRepository = mockRepository;
		}};
	}
	
	@Test
	public void calcularMelhorRotaUsandoConsultaDoRepositorio() {
		Mapa mapa = new Mapa();
		Ponto origem = new Ponto();
		Ponto destino = new Ponto();
		
		calculoRota.calcularMelhorRota(mapa, origem, destino);
		
		verify(mockRepository, times(1)).findMelhorRota(mapa, origem, destino);		
	}
	
}
