package com.wallmart.logistica.service;

import java.math.BigDecimal;

import com.wallmart.logistica.domain.Mapa;
import com.wallmart.logistica.domain.Ponto;
import com.wallmart.logistica.domain.RotaCompleta;

public interface CalculoRotaService {

	RotaCompleta calcularMelhorRota(Mapa mapa, Ponto origem, Ponto destino);

	BigDecimal calcularCusto(Integer distancia, BigDecimal valorLitro, BigDecimal autonomia); 
	
}
