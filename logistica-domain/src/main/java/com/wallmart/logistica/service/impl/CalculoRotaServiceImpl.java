package com.wallmart.logistica.service.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.wallmart.logistica.domain.Mapa;
import com.wallmart.logistica.domain.Ponto;
import com.wallmart.logistica.domain.RotaCompleta;
import com.wallmart.logistica.repository.MapaRepository;
import com.wallmart.logistica.service.CalculoRotaService;

@Stateless
public class CalculoRotaServiceImpl implements CalculoRotaService {

	@Inject
	protected MapaRepository mapaRepository;
	
	@Override
	public RotaCompleta calcularMelhorRota(Mapa mapa, Ponto origem, Ponto destino) {		
		RotaCompleta melhorRota = mapaRepository.findMelhorRota(mapa, origem, destino);
		if (melhorRota != null) {
			melhorRota.calcularPontos();
			melhorRota.calcularDistanciaTotal();
		}
		return melhorRota;
	}

	@Override
	public BigDecimal calcularCusto(Integer distancia, BigDecimal valorLitro, BigDecimal autonomia) {
		BigDecimal litrosNecessarios = new BigDecimal(distancia).divide(autonomia, 4, RoundingMode.HALF_EVEN);
		BigDecimal custo = litrosNecessarios.multiply(valorLitro);
		return custo.setScale(2, RoundingMode.HALF_EVEN);
	}
	
}
