package com.wallmart.logistica.repository;

import com.wallmart.logistica.domain.Mapa;
import com.wallmart.logistica.domain.Ponto;
import com.wallmart.logistica.domain.Rota;
import com.wallmart.logistica.domain.RotaCompleta;

public interface MapaRepository {
	
	Mapa store(Mapa mapa);
	
	Mapa findByNome(String nomeMapa);
	
	Ponto findPontoByNome(String nomeMapa, String nomePonto);
	
	RotaCompleta findMelhorRota(Mapa mapa, Ponto origem, Ponto destino);
	
	Rota storeRota(Rota rota);
	
}
