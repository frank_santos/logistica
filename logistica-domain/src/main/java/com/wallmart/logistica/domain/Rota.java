package com.wallmart.logistica.domain;

import java.io.Serializable;

import org.neo4j.ogm.annotation.EndNode;
import org.neo4j.ogm.annotation.RelationshipEntity;
import org.neo4j.ogm.annotation.StartNode;

@RelationshipEntity(type = "ROTA")
public class Rota implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Long id;
	
	@StartNode
	private Ponto origem;
	
	@EndNode
	private Ponto destino;
	
	private Integer distancia;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Ponto getOrigem() {
		return origem;
	}
	
	public void setOrigem(Ponto origem) {
		this.origem = origem;
	}
	
	public Ponto getDestino() {
		return destino;
	}
	
	public void setDestino(Ponto destino) {
		this.destino = destino;
	}
	
	public Integer getDistancia() {
		return distancia;
	}
	
	public void setDistancia(Integer distancia) {
		this.distancia = distancia;
	}
	
	@Override
	public String toString() {		
		return "Rota {origem: " + origem + ", destino: " + destino + "}";
	}
}
