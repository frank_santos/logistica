package com.wallmart.logistica.domain;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class RotaCompleta implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private List<Rota> subRotas;
	private Integer distanciaTotal;
	private Ponto[] pontos;
	
	public List<Rota> getSubRotas() {
		return subRotas;
	}
	
	public void setSubRotas(List<Rota> subRotas) {
		this.subRotas = subRotas;
	}
	
	public Integer getDistanciaTotal() {		
		return distanciaTotal;
	}
	
	public void setDistanciaTotal(Integer distinciaTotal) {
		this.distanciaTotal = distinciaTotal;
	}
		
	public void calcularDistanciaTotal() {
		distanciaTotal = 0;
		if (subRotas != null) {
			subRotas.forEach(it -> distanciaTotal += it.getDistancia());
		}
	}
	
	public void calcularPontos() {
		Set<Ponto> set = new LinkedHashSet<>();
		if (subRotas != null) {
			subRotas.forEach(it -> {
				set.add(it.getOrigem());
				set.add(it.getDestino());
			});
		}
		pontos = set.toArray(new Ponto[]{});
	}

	public Ponto[] getPontos() {
		return pontos;
	}
	
}
