package com.wallmart.logistica.domain;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Optional;
import java.util.Set;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

@NodeEntity
public class Mapa implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@GraphId
	private Long id;
	private String nome;
	
	@Relationship(type = "PONTO")
	private Set<Ponto> pontos = new LinkedHashSet<>();
	
	public Mapa() {
		super();
	}
	
	public Mapa(String nome) {
		super();
		this.nome = nome;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Set<Ponto> getPontos() {
		return pontos;
	}
	
	public void setPontos(Set<Ponto> pontos) {
		this.pontos = pontos;
	}
	
	public Ponto addPonto(String nomePonto) {
		Ponto ponto = new Ponto();
		ponto.setNome(nomePonto);
		pontos.add(ponto);
		return ponto;
	}
	
	public Ponto getOrAddPonto(String nomePonto) {
		Ponto ponto = getPonto(nomePonto);
		if (ponto == null) {
			ponto = addPonto(nomePonto);			
		}
		return ponto;
	}
	
	public Ponto getPonto(String nomePonto) {
		Optional<Ponto> first = pontos.stream().filter(it -> it.getNome().equals(nomePonto)).findFirst();
		
		return first.isPresent() ? first.get() : null;		
	}
	
	@Override
	public String toString() {		
		return "Mapa {nome: " + nome + "}";
	}

	public Ponto[] getPontosAsArray() {
		if (pontos == null) {
			return null;
		}
		return pontos.toArray(new Ponto[]{});
	}
	
}
