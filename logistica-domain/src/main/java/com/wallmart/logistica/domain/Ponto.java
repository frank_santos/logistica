package com.wallmart.logistica.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

@NodeEntity
public class Ponto implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@GraphId
	private Long id;
	private String nome;
	@Relationship(type = "ROTA", direction = Relationship.INCOMING)
	private List<Rota> rotas = new ArrayList<>();
	
	public Ponto() {
		super();
	}
	
	public Ponto(String nome) {
		super();
		this.nome = nome;
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public List<Rota> getRotas() {
		return rotas;
	}
	
	public void setRotas(List<Rota> rotas) {
		this.rotas = rotas;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || !(obj instanceof Ponto) || id == null) {
			return false;
		}
		Ponto other = (Ponto) obj;
		
		return (!id.equals(other.id));			
	}
	
	@Override
	public String toString() {
		return "Ponto {id: " + id + ", nome: '" + nome + "'}";
	}

	public void addRota(Ponto destino, Integer distancia) {
		Rota rota = getRota(destino);
		
		if (rota == null)  {
			rota = new Rota();
		}
		
		rota.setOrigem(this);
		rota.setDestino(destino);
		rota.setDistancia(distancia);
		
		rotas.add(rota );
	}
	
	public Rota getRota(Ponto destino) {
		Optional<Rota> first = rotas.stream().filter(it -> it.getDestino().equals(destino)).findFirst();
		
		return first.isPresent() ? first.get() : null;
	}
	
}
