Wallmart - Logistica

Requisitos:

    O Walmart esta desenvolvendo um novo sistema de logistica e sua ajuda é muito importante neste momento. Sua tarefa será desenvolver o novo sistema de entregas visando sempre o menor custo. Para popular sua base de dados o sistema precisa expor um Webservices que aceite o formato de malha logística (exemplo abaixo), nesta mesma requisição o requisitante deverá informar um nome para este mapa. É importante que os mapas sejam persistidos para evitar que a cada novo deploy todas as informações desapareçam. O formato de malha logística é bastante simples, cada linha mostra uma rota: ponto de origem, ponto de destino e distância entre os pontos em quilômetros.

    A B 10
    B D 15
    A C 20
    C D 30
    B E 50
    D E 30

    Com os mapas carregados o requisitante irá procurar o menor valor de entrega e seu caminho, para isso ele passará o mapa, nome do ponto de origem, nome do ponto de destino, autonomia do caminhão (km/l) e o valor do litro do combustivel, agora sua tarefa é criar este Webservices. Um exemplo de entrada seria, mapa SP, origem A, destino D, autonomia 10, valor do litro 2,50; a resposta seria a rota A B D com custo de 6,25.

Tecnologias Utilizadas:

  Linguagem de Programação: Java 8 (Mais recente versão do Java que possui muitos recursos que aumentam a produtividade) 

  Gerenciamento de artefatos e dependências: Maven (Padrão de mercado)

  Banco de Dados: Neo4j (Ideal para trabalhar com informações interligadas)

  Plataforma de desenvolvimento e execução: Java EE 7 (Plataforma padrão do mercado para desenvolvimento de aplicações corporativas)

  Frameworks de Implementação:
    CDI (Framework para Injecão de Dependência padrão da Plataforma Java EE)
    EJB (framework disponível no Java EE para implementar componentes da camada de negócios)
    Neo4j-ogm (Facilita a persistência no banco de dados Neo4j utilizando anotações e uma API de fácil utilização)
	SOAP WebServices (Protocolo consolidado para WebServices focado em contrato de serviços (WSDL), facilitando o consumo pelo clientes)
	JAX-WS (API padrão do Java para trabalhar com WebServices SOAP)

  Servidor de Aplicação: 
    Grassfish 4.x (Implementação de referência para a plataforma Java EE 7)

Arquitetura do Sistema:

  Usado o modelo de arquitetura recomendado pelo DDD (Domain Driven Design) dividindo o sistema em 3 cadadas:
  
  - logistica-infra:  camada responsável pela implementação de serviços de infraestratura como persistência
  - logistica-domain: camada separada para o modelo de dominio da aplicação. componentes: entidades, interface dos repositórios e serviços de negócio (regra de negócio)
  - logistica-app:    camada responsável por serviços de aplicação, tarefas, fachadas e pela comunição de sistemas externos com a aplicação


Montagem de Ambiente:

  Instalar o Neo4J versão 2.3.2 (configurar usuario "neo4j" e senha "adminadmin")  
  Iniciar o Neo4J 
  Baixar o Glassifsh 4.1.1 Full (http://download.java.net/glassfish/4.1.1/release/glassfish-4.1.1.zip)
  Iniciar o Glassfish (glassfish4/bin/asadmin start-domain)
  Gerar o pacote da aplicação: mvn package 
  Fazer o deploy do arquivo "logistica-app\target\logistica-app-0.0.1-SNAPSHOT.war" no Glassfish
    Abrir no navegador a URL: http://localhost:4848/
	Applications -> Deploy -> Selecionar o arquivo -> OK

Endereço dos WebServices:

  http://localhost:8080/logistica/CadastroMalhaWS?wsdl 
  http://localhost:8080/logistica/CalculoRotaWS?wsdl
