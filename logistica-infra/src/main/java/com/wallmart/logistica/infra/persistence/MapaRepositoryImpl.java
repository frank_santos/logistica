package com.wallmart.logistica.infra.persistence;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.neo4j.ogm.model.Result;
import org.neo4j.ogm.session.Session;

import com.wallmart.logistica.domain.Mapa;
import com.wallmart.logistica.domain.Ponto;
import com.wallmart.logistica.domain.Rota;
import com.wallmart.logistica.domain.RotaCompleta;
import com.wallmart.logistica.repository.MapaRepository;

@Stateless
public class MapaRepositoryImpl implements MapaRepository {
	
	@Inject Session session;
	
	@Override
	public Mapa store(Mapa mapa) {
		session.save(mapa);
		
		return mapa;
	}
	
	@Override
	public Rota storeRota(Rota rota) {
		session.save(rota);
		
		return rota;
	}
	
	@Override
	public Mapa findByNome(String nome) {
		Map<String, Object> params = new HashMap<>();
		params.put("nome", nome);
		
		String query = "MATCH (m:Mapa) WHERE m.nome = {nome} RETURN m";
		
		Mapa mapa = session.queryForObject(Mapa.class, query, params);
		
		return mapa;
	}
	
	@Override
	public Ponto findPontoByNome(String nomeMapa, String nomePonto) {
		Map<String, Object> params = new HashMap<>();
		params.put("nomeMapa", nomeMapa);
		params.put("nomePonto", nomePonto);
		
		String query = "MATCH (m:Mapa)-->(p:Ponto) WHERE m.nome = {nomeMapa} AND p.nome = {nomePonto} RETURN p";
		
		Ponto ponto = session.queryForObject(Ponto.class, query, params);
		
		return ponto;
	}
	
	@Override
	public RotaCompleta findMelhorRota(Mapa mapa, Ponto origem, Ponto destino) {
		String query = 
				"MATCH (m:Mapa {nome:{mapa}})-[p:PONTO]->(p1:Ponto{nome:{origem}})-[r:ROTA*]->(p2:Ponto {nome:{destino}}) " +
				"WITH r, reduce(s = 0, rel IN r | s + rel.distancia) as distanciaTotal " +
				"ORDER BY distanciaTotal LIMIT 1 " +
				"UNWIND r AS rota RETURN startNode(rota) AS origem, endNode(rota) AS destino, rota.distancia AS distancia";
			
		Map<String, Object> params = new HashMap<>();
		params.put("mapa", mapa.getNome());
		params.put("origem", origem.getNome());
		params.put("destino", destino.getNome());
		
		Result result = session.query(query, params);
		RotaCompleta rotaCompleta = new RotaCompleta();
		rotaCompleta.setSubRotas(new ArrayList<>());
		result.forEach(it -> {
			Rota subRota = new Rota();
			subRota.setOrigem((Ponto) it.get("origem"));
			subRota.setDestino((Ponto) it.get("destino"));
			subRota.setDistancia((Integer) it.get("distancia"));
			rotaCompleta.getSubRotas().add(subRota);
		});
		
		if (rotaCompleta.getSubRotas().isEmpty()) {
			return null;
		}
		
		return rotaCompleta;
	}
	
	
}
