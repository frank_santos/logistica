package com.wallmart.logistica.infra.persistence;

import java.io.Serializable;

import javax.enterprise.inject.Produces;
import javax.inject.Singleton;

import org.neo4j.ogm.session.Session;
import org.neo4j.ogm.session.SessionFactory;

@Singleton
public class Neo4jSessionFactory implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private SessionFactory sessionFactory = new SessionFactory("com.wallmart.logistica.domain");
	
	@Produces
	public Session createSession() {			
		return sessionFactory.openSession();
	}

}
