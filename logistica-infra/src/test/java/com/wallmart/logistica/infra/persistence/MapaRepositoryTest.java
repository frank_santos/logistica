package com.wallmart.logistica.infra.persistence;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.neo4j.ogm.config.DriverConfiguration;
import org.neo4j.ogm.service.Components;
import org.neo4j.ogm.session.Session;

import com.wallmart.logistica.domain.Mapa;
import com.wallmart.logistica.domain.Ponto;
import com.wallmart.logistica.domain.Rota;
import com.wallmart.logistica.domain.RotaCompleta;
import com.wallmart.logistica.repository.MapaRepository;

public class MapaRepositoryTest {
	
	private static MapaRepository mapaRepository;
	private static Session session;
	
	@BeforeClass
	public static void init() throws Exception {
		
		DriverConfiguration driverConfiguration = Components.configuration().driverConfiguration();
		driverConfiguration.setDriverClassName("org.neo4j.ogm.drivers.embedded.driver.EmbeddedDriver");		             
		
		session = new Neo4jSessionFactory().createSession();
		importData();
		mapaRepository = new MapaRepositoryImpl() {{
			this.session = MapaRepositoryTest.session;
		}};		
	}

	private static void importData() throws IOException, URISyntaxException {
		URL queryResource = Thread.currentThread().getContextClassLoader().getResource("logistica.cql");
		byte[] queryBytes = Files.readAllBytes(Paths.get(queryResource.toURI()));
		String query = new String(queryBytes);
		session.query(query, Collections.emptyMap());
	}
	
	@AfterClass
	public static void shutdown() {
		session.purgeDatabase();
	}
	
	@Test
	public void gravarUmMapaSimples() {
		String nomeMapa = "Mapa Simples";
		
		Mapa mapa = new Mapa();
		mapa.setNome(nomeMapa);
		
		mapaRepository.store(mapa);
		
		Mapa test = mapaRepository.findByNome(nomeMapa);
		assertNotNull(test);
		assertEquals(nomeMapa, test.getNome());		
	}
	
	@Test
	public void gravarUmMapaCom2Pontos() {
		String nomeMapa = "Mapa Com Pontos";
		
		Mapa mapa = new Mapa();
		mapa.setNome(nomeMapa);
		mapa.addPonto("Ponto 1");
		mapa.addPonto("Ponto 2");
		
		mapaRepository.store(mapa);
		
		Mapa found = mapaRepository.findByNome(nomeMapa);
		assertNotNull(found);
		assertEquals(nomeMapa, found.getNome());
		Ponto[] pontos = found.getPontos().toArray(new Ponto[]{});
		assertEquals(2, pontos.length);
		assertEquals("Ponto 1", pontos[0].getNome());
		assertEquals("Ponto 2", pontos[1].getNome());
	}
	
	@Test
	public void gravarUmMapaComPontosERotas() {
		String nomeMapa = "Mapa Com Pontos e Rotas";
		
		Mapa mapa = new Mapa();
		mapa.setNome(nomeMapa);
		Ponto ponto1 = mapa.addPonto("Ponto 10");
		Ponto ponto2 = mapa.addPonto("Ponto 20");
		
		ponto1.addRota(ponto2, 75);
		
		mapaRepository.store(mapa);
		
		Mapa found = mapaRepository.findByNome(nomeMapa);
		assertNotNull(found);
		assertEquals(nomeMapa, found.getNome());		
		Ponto[] pontos = found.getPontosAsArray();
		assertEquals(2, pontos.length);
		assertEquals("Ponto 10", pontos[0].getNome());
		assertEquals("Ponto 20", pontos[1].getNome());
		
		Ponto ponto1Found = pontos[0];		
		assertEquals(1, ponto1Found.getRotas().size());
		
		Ponto ponto2Found = pontos[1];
		assertEquals(0, ponto2Found.getRotas().size());
	}
	
	@Test
	public void localizarUmMapaPeloNome() {
		Mapa found = mapaRepository.findByNome("SP");
		
		assertNotNull(found);
		assertEquals("SP", found.getNome());		
	}
	
	@Test
	public void localizarUmPontoPeloMapaENome() {
		Ponto found = mapaRepository.findPontoByNome("SP", "C");
		
		assertNotNull(found);
		assertEquals("C", found.getNome());
	}
	
	@Test
	public void localizarMelhorRotaDosPontosAD() {
		Mapa mapa = new Mapa("SP");
		Ponto origem = new Ponto("A");
		Ponto destino = new Ponto("D");
		
		RotaCompleta rotaCompleta = mapaRepository.findMelhorRota(mapa, origem, destino);
		
		assertNotNull(rotaCompleta);
		assertNotNull(rotaCompleta.getSubRotas());
		assertEquals(2, rotaCompleta.getSubRotas().size());
		
		Rota rota1 = rotaCompleta.getSubRotas().get(0);
		Rota rota2 = rotaCompleta.getSubRotas().get(1);
		
		assertEquals("A", rota1.getOrigem().getNome());
		assertEquals("B", rota1.getDestino().getNome());
		assertEquals("B", rota2.getOrigem().getNome());
		assertEquals("D", rota2.getDestino().getNome());
	}
	
	@Test
	public void localizarMelhorRotaDosPontosBE() {
		Mapa mapa = new Mapa("SP");
		Ponto origem = new Ponto("B");
		Ponto destino = new Ponto("E");
		
		RotaCompleta rotaCompleta = mapaRepository.findMelhorRota(mapa, origem, destino);
		
		assertNotNull(rotaCompleta);
		assertNotNull(rotaCompleta.getSubRotas());
		assertEquals(2, rotaCompleta.getSubRotas().size());
		
		Rota rota1 = rotaCompleta.getSubRotas().get(0);
		Rota rota2 = rotaCompleta.getSubRotas().get(1);
		
		assertEquals("B", rota1.getOrigem().getNome());
		assertEquals("D", rota1.getDestino().getNome());
		assertEquals("D", rota2.getOrigem().getNome());
		assertEquals("E", rota2.getDestino().getNome());
	}
	
	@Test
	public void localizarMelhorRotaSemResultado() {
		Mapa mapa = new Mapa("SP");
		Ponto origem = new Ponto("F");
		Ponto destino = new Ponto("G");
		
		RotaCompleta rotaCompleta = mapaRepository.findMelhorRota(mapa, origem, destino);
		
		assertNull(rotaCompleta);
	}
	
	
}
