package com.wallmart.logistica.app.exception;

public class MapaUnknownException extends ApplicationException {

	private static final long serialVersionUID = 1L;

	public MapaUnknownException() {
		super();
	}

	public MapaUnknownException(String message, Throwable cause) {
		super(message, cause);
	}

	public MapaUnknownException(String message) {
		super(message);
	}

	public MapaUnknownException(Throwable cause) {
		super(cause);
	}

}
