package com.wallmart.logistica.app.message;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class RotaDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@XmlElement(required = true)
	private String origem;
	
	@XmlElement(required = true)
	private String destino;
	
	@XmlElement(required = true)
	private Integer distancia;
	
	public RotaDTO() {
		super();
	}
	
	public RotaDTO(String origem, String destino, Integer distancia) {
		super();
		this.origem = origem;
		this.destino = destino;
		this.distancia = distancia;
	}

	public String getOrigem() {
		return origem;
	}
	
	public void setOrigem(String origem) {
		this.origem = origem;
	}
	
	public String getDestino() {
		return destino;
	}
	
	public void setDestino(String destino) {
		this.destino = destino;
	}
	
	public Integer getDistancia() {
		return distancia;
	}
	
	public void setDistancia(Integer distancia) {
		this.distancia = distancia;
	}
	
}
