package com.wallmart.logistica.app.message;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class CadastroMalhaRequest implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@XmlElement(required = true)
	private String nomeMapa;
	
	@XmlElementWrapper(name = "rotas", required = true)
	@XmlElement(name = "rota", required = true)
	private List<RotaDTO> rotas;
	
	public String getNomeMapa() {
		return nomeMapa;
	}
	
	public void setNomeMapa(String nomeMapa) {
		this.nomeMapa = nomeMapa;
	}
	
	public List<RotaDTO> getRotas() {
		return rotas;
	}
	
	public void setRotas(List<RotaDTO> rotas) {
		this.rotas = rotas;
	}
	
}
