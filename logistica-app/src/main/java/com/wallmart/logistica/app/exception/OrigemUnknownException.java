package com.wallmart.logistica.app.exception;

public class OrigemUnknownException extends ApplicationException {

	private static final long serialVersionUID = 1L;

	public OrigemUnknownException() {
		super();
	}

	public OrigemUnknownException(String message, Throwable cause) {
		super(message, cause);
	}

	public OrigemUnknownException(String message) {
		super(message);
	}

	public OrigemUnknownException(Throwable cause) {
		super(cause);
	}

}
