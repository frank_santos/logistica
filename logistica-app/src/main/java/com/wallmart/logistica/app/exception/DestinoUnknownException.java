package com.wallmart.logistica.app.exception;

public class DestinoUnknownException extends ApplicationException {

	private static final long serialVersionUID = 1L;

	public DestinoUnknownException() {
		super();
	}

	public DestinoUnknownException(String message, Throwable cause) {
		super(message, cause);
	}

	public DestinoUnknownException(String message) {
		super(message);
	}

	public DestinoUnknownException(Throwable cause) {
		super(cause);
	}

}
