package com.wallmart.logistica.app.message;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class CalculoRotaRequest implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@XmlElement(required = true)
	private String mapa;
	
	@XmlElement(required = true)
	private String origem;
	
	@XmlElement(required = true)
	private String destino;
	
	@XmlElement(required = true)
	private BigDecimal autonomia;
	
	@XmlElement(required = true)
	private BigDecimal valorLitro;
	
	public String getMapa() {
		return mapa;
	}
	public void setMapa(String mapa) {
		this.mapa = mapa;
	}
	public String getOrigem() {
		return origem;
	}
	public void setOrigem(String origem) {
		this.origem = origem;
	}
	public String getDestino() {
		return destino;
	}
	public void setDestino(String destino) {
		this.destino = destino;
	}
	public BigDecimal getAutonomia() {
		return autonomia;
	}
	public void setAutonomia(BigDecimal autonomia) {
		this.autonomia = autonomia;
	}
	public BigDecimal getValorLitro() {
		return valorLitro;
	}
	public void setValorLitro(BigDecimal valorLitro) {
		this.valorLitro = valorLitro;
	}
	
}
