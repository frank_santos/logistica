package com.wallmart.logistica.app.ws;

import java.math.BigDecimal;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import com.wallmart.logistica.app.exception.ApplicationException;
import com.wallmart.logistica.app.exception.DestinoUnknownException;
import com.wallmart.logistica.app.exception.MapaUnknownException;
import com.wallmart.logistica.app.exception.OrigemUnknownException;
import com.wallmart.logistica.app.exception.RotaUnknownException;
import com.wallmart.logistica.app.message.CalculoRotaRequest;
import com.wallmart.logistica.app.message.CalculoRotaResponse;
import com.wallmart.logistica.domain.Mapa;
import com.wallmart.logistica.domain.Ponto;
import com.wallmart.logistica.domain.RotaCompleta;
import com.wallmart.logistica.repository.MapaRepository;
import com.wallmart.logistica.service.CalculoRotaService;

@Stateless
@WebService ( serviceName = "logistica", name = "CalculoRotaWS")
public class CalculoRotaWS {
	
	@Inject MapaRepository mapaRepository;
	@Inject CalculoRotaService calculoRotaService;
	
	@WebMethod
	@SOAPBinding(parameterStyle=SOAPBinding.ParameterStyle.BARE)
	public CalculoRotaResponse calcularMelhorRota(CalculoRotaRequest request) throws ApplicationException {
		Mapa mapa = mapaRepository.findByNome(request.getMapa());
		Ponto origem = mapaRepository.findPontoByNome(request.getMapa(), request.getOrigem());
		Ponto destino = mapaRepository.findPontoByNome(request.getMapa(), request.getDestino());
		
		if (mapa == null) {
			throw new MapaUnknownException("Mapa não encontrado");
		}
		if (origem == null) {
			throw new OrigemUnknownException("Origem não encontrada");
		}
		if (destino == null) {
			throw new DestinoUnknownException("Destino não encontrado");
		}
		
		RotaCompleta rota = calculoRotaService.calcularMelhorRota(mapa , origem, destino);
		
		if (rota == null) {
			throw new RotaUnknownException("Nenhuma rota encontrada");
		}
		CalculoRotaResponse response = buildResponse(rota);
		
		BigDecimal custo = calculoRotaService.calcularCusto(rota.getDistanciaTotal(), request.getValorLitro(), request.getAutonomia());
		response.setCusto(custo);
		
		return response;
	}

	private CalculoRotaResponse buildResponse(RotaCompleta rota) {
		CalculoRotaResponse response = new CalculoRotaResponse();
		
		if (rota.getPontos() != null) {
			String[] pontosNome = new String[rota.getPontos().length];
			
			for (int i = 0; i < pontosNome.length; i++) {
				pontosNome[i] = rota.getPontos()[i].getNome();
			}
		
			response.setRota(pontosNome);
		}
		return response;
	}
	
}
