package com.wallmart.logistica.app.ws;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import com.wallmart.logistica.app.message.CadastroMalhaRequest;
import com.wallmart.logistica.app.message.CadastroMalhaResponse;
import com.wallmart.logistica.domain.Mapa;
import com.wallmart.logistica.domain.Ponto;
import com.wallmart.logistica.repository.MapaRepository;

@Stateless
@WebService(serviceName = "logistica", name = "CadastroMalhaWS")
public class CadastroMalhaWS {
	
	@Inject MapaRepository mapaRepository;
	
	@WebMethod
	@SOAPBinding(parameterStyle=SOAPBinding.ParameterStyle.BARE)
	public CadastroMalhaResponse gravarMalha(CadastroMalhaRequest request) {
		
		CadastroMalhaResponse response = new CadastroMalhaResponse();
		
		Mapa mapa = new Mapa();
		mapa.setNome(request.getNomeMapa());
		request.getRotas().forEach(rotaDTO -> {
			Ponto origem = mapa.getOrAddPonto(rotaDTO.getOrigem());
			Ponto destino = mapa.getOrAddPonto(rotaDTO.getDestino());
			origem.addRota(destino, rotaDTO.getDistancia());
		});
		
		mapaRepository.store(mapa);
		response.setSucesso(true);
		response.setMensagem("Malha logistica gravada com sucesso.");
		
		return response ;
	}
	
}
