package com.wallmart.logistica.app.message;

import java.io.Serializable;

public class CadastroMalhaResponse implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Boolean sucesso;
	private String mensagem;
	
	public Boolean getSucesso() {
		return sucesso;
	}
	
	public void setSucesso(Boolean sucesso) {
		this.sucesso = sucesso;
	}
	
	public String getMensagem() {
		return mensagem;
	}
	
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
}
