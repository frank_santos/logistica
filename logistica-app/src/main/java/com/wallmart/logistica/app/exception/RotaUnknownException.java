package com.wallmart.logistica.app.exception;

public class RotaUnknownException extends ApplicationException {

	private static final long serialVersionUID = 1L;

	public RotaUnknownException() {
		super();
	}

	public RotaUnknownException(String message, Throwable cause) {
		super(message, cause);
	}

	public RotaUnknownException(String message) {
		super(message);
	}

	public RotaUnknownException(Throwable cause) {
		super(cause);
	}

}
