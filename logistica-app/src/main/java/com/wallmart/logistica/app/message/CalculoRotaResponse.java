package com.wallmart.logistica.app.message;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

@XmlAccessorType(XmlAccessType.FIELD)
public class CalculoRotaResponse implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@XmlElementWrapper(name = "rota")
	@XmlElement(name = "ponto")
	private String[] rota;
	private BigDecimal custo;
	
	public String[] getRota() {
		return rota;
	}
	
	public void setRota(String[] rota) {
		this.rota = rota;
	}
	
	public BigDecimal getCusto() {
		return custo;
	}
	
	public void setCusto(BigDecimal custo) {
		this.custo = custo;
	}
	
}
