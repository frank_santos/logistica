package com.wallmart.logistica.app.ws;


import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.wallmart.logistica.app.exception.ApplicationException;
import com.wallmart.logistica.app.exception.DestinoUnknownException;
import com.wallmart.logistica.app.exception.MapaUnknownException;
import com.wallmart.logistica.app.exception.OrigemUnknownException;
import com.wallmart.logistica.app.exception.RotaUnknownException;
import com.wallmart.logistica.app.message.CalculoRotaRequest;
import com.wallmart.logistica.domain.Mapa;
import com.wallmart.logistica.domain.Ponto;
import com.wallmart.logistica.domain.RotaCompleta;
import com.wallmart.logistica.repository.MapaRepository;
import com.wallmart.logistica.service.CalculoRotaService;

public class CalculoRotaWSTest {
	
	private CalculoRotaWS calculoRotaWS;
	private CalculoRotaService mockService;
	private MapaRepository mockRepository;
	
	private Mapa foundMapa = new Mapa("Mapa");
	private Ponto foundOrigem = new Ponto("Origem");
	private Ponto foundDestino = new Ponto("Destino");
	private RotaCompleta foundRota = new RotaCompleta();
	private CalculoRotaRequest request = new CalculoRotaRequest();
	
	public static final String UNKNOWN = "(unknown)";
	
	@Before
	public void init() {
		mockService = Mockito.mock(CalculoRotaService.class);
		mockRepository = Mockito.mock(MapaRepository.class);
		calculoRotaWS = new CalculoRotaWS(){{
			this.mapaRepository = mockRepository;
			this.calculoRotaService = mockService;
		}};
				
	}
		
	private void mockValuesAndCalcularMelhorRota() throws ApplicationException {
		mockReturnValues();
		calculoRotaWS.calcularMelhorRota(request);		
	}
	
	@Test(expected = MapaUnknownException.class)
	public void calcularRotaComMapaNaoEncontrado() throws ApplicationException {
		request.setMapa(UNKNOWN);
		request.setOrigem("Qualquer");
		request.setDestino("Qualquer");
		
		mockValuesAndCalcularMelhorRota();		
	}
		
	@Test(expected = OrigemUnknownException.class)
	public void calcularRotaComOrigemNaoEncontrada() throws ApplicationException {
		request.setMapa("Qualquer");
		request.setOrigem(UNKNOWN);
		request.setDestino("Qualquer");
		
		mockValuesAndCalcularMelhorRota();
	}
	
	@Test(expected = DestinoUnknownException.class)
	public void calcularRotaComDestinoNaoEncontrado() throws ApplicationException {		
		request.setMapa("Qualquer");
		request.setOrigem("Qualquer");
		request.setDestino(UNKNOWN);
		
		mockValuesAndCalcularMelhorRota();
	}
	
	@Test(expected = RotaUnknownException.class)
	public void calcularRotaNaoEncontrada() throws ApplicationException {
		
		request.setMapa("Qualquer");
		request.setOrigem("Qualquer Origem");
		request.setDestino("Qualquer Destino");
		foundRota = null;
		
		mockValuesAndCalcularMelhorRota();
	}
	
	@Test
	public void calcularRotaComSucesso() throws ApplicationException {
		
		request.setMapa("Qualquer Mapa");
		request.setOrigem("Qualquer Origem");
		request.setDestino("Qualquer Destino");
		request.setValorLitro(new BigDecimal("3.15"));
		request.setAutonomia(new BigDecimal("15.00"));
		foundRota.setDistanciaTotal(75);
		
		mockValuesAndCalcularMelhorRota();
		
		verify(mockService, times(1)).calcularMelhorRota(foundMapa, foundOrigem, foundDestino);
		verify(mockService, times(1)).calcularCusto(foundRota.getDistanciaTotal(), request.getValorLitro(), request.getAutonomia());
	}
	
	private void mockReturnValues() {
		foundMapa = verifyUnknowValue(foundMapa, request.getMapa());
		foundOrigem = verifyUnknowValue(foundOrigem, request.getOrigem());
		foundDestino = verifyUnknowValue(foundDestino, request.getDestino());
		
		when(mockRepository.findByNome(request.getMapa())).thenReturn(foundMapa);
		when(mockRepository.findPontoByNome(request.getMapa(), request.getOrigem())).thenReturn(foundOrigem);
		when(mockRepository.findPontoByNome(request.getMapa(), request.getDestino())).thenReturn(foundDestino);
		when(mockService.calcularMelhorRota(foundMapa, foundOrigem, foundDestino)).thenReturn(foundRota);
	}

	private <T> T verifyUnknowValue(T value, String desc) {
		if (UNKNOWN.equals(desc)) {
			return null;
		}
		return value;
	}
	
}
