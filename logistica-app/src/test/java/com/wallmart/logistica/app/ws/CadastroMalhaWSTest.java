package com.wallmart.logistica.app.ws;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import com.wallmart.logistica.app.message.CadastroMalhaRequest;
import com.wallmart.logistica.app.message.CadastroMalhaResponse;
import com.wallmart.logistica.app.message.RotaDTO;
import com.wallmart.logistica.domain.Mapa;
import com.wallmart.logistica.repository.MapaRepository;

public class CadastroMalhaWSTest {

	private CadastroMalhaWS cadastroMalhaWS;
	private MapaRepository mockRepository;
	
	@Before
	public void init() {
		mockRepository = Mockito.mock(MapaRepository.class);
		cadastroMalhaWS = new CadastroMalhaWS(){{
			this.mapaRepository = mockRepository;
		}};
	}
	
	@Test
	public void gravarMalharEValidarParametrosParaORepositorio() {
		CadastroMalhaRequest input = buildInput();
		
		ArgumentCaptor<Mapa> captor = ArgumentCaptor.forClass(Mapa.class);
		
		cadastroMalhaWS.gravarMalha(input);
		
		verify(mockRepository).store(captor.capture());
		
		Mapa mapa = captor.getValue();
		
		assertEquals("SP", mapa.getNome());		
		assertNotNull(mapa.getPontos());
		assertEquals(5, mapa.getPontos().size());		
		assertNotNull(mapa.getPonto("A"));
		assertNotNull(mapa.getPonto("B"));
		assertNotNull(mapa.getPonto("C"));
		assertNotNull(mapa.getPonto("D"));
		assertNotNull(mapa.getPonto("E"));		
	}
	
	@Test
	public void gravarMalharCompletaComSucesso() {
		CadastroMalhaRequest input = buildInput();
		
		CadastroMalhaResponse output = cadastroMalhaWS.gravarMalha(input);
		
		verify(mockRepository, times(1)).store(any());
		assertEquals(true, output.getSucesso());
	}
	
	private CadastroMalhaRequest buildInput() {
		CadastroMalhaRequest input = new CadastroMalhaRequest();
		input.setNomeMapa("SP");
		input.setRotas(Arrays.asList(
			new RotaDTO("A", "B", 10),
			new RotaDTO("B", "D", 15),
			new RotaDTO("A", "C", 20),
			new RotaDTO("C", "D", 30),
			new RotaDTO("B", "E", 50),
			new RotaDTO("D", "E", 30)
		));
		return input;
	}
	
}
